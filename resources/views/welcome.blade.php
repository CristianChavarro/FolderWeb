﻿<!DOCTYPE html>
<html>
<head>
    <!-- Title and meta tag -->
    <title>One Page Responsive Site Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" href="assets/images/favicon.ico" />
    <meta name="description" content="YOUR DESCRIPTION" />
    <meta name="author" content="YOURNAME" />
    <meta name="keywords" content="YOUR KEYWORDS" />
    <!-- Facebook Metadata -->
    <meta property="fb:page_id" content="" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="" />
    <meta property="og:title" content="" />
    <!-- StyleSheets -->
    <link rel="stylesheet" type="text/css" href="assets/styles/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="assets/styles/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="assets/styles/core.css" />
    <!-- Scripts -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/scripts/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="assets/scripts/jquery.localscroll-1.2.7-min.js"></script>
    <script type="text/javascript" src="assets/scripts/jquery.scrollTo-1.4.2-min.js"></script>
    <script type="text/javascript" src="assets/scripts/jquery.validate.pack.js"></script>
    <script type="text/javascript" src="assets/scripts/jquery.cycle.all.js"></script>
    <script type="text/javascript" src="assets/scripts/jquery.maximage.js"></script>
    <script type="text/javascript" src="assets/scripts/custom.js"></script>
    <script type="text/javascript">
        function loading() {
            setTimeout('loadingVisible()', 2500);
        }

        function loadingVisible() {
            $('body').css('overflow', 'inherit');
            $("#loadingWrap").fadeOut('slow');
        }
    </script>
</head>
<body onload="loading();">
    <div id="loadingWrap">
        <div id="circleG">
            <div id="circleG_1" class="circleG">
            </div>
            <div id="circleG_2" class="circleG">
            </div>
            <div id="circleG_3" class="circleG">
            </div>
        </div>
    </div>
    <div class="mainWrap">
        <div class="mainMenuWrap">
            <div class="container">
                <img class="logo" src="assets/images/Logo.png" alt="Logo" />
                <ul id="siteMenu" class="mainMenu hidden-phone">
                    <li><a class="active" href="#homePage">HOME</a></li>
                    <li><a href="#aboutUs">WHO WE ARE</a></li>
                    <li><a href="#works">WORKS</a></li>
                    <li><a href="#contact">CONTACT</a></li>
                    <li><a href="#" target="_blank">BUY NOW</a></li>
                </ul>
                <select class="mainMenu visible-phone">
                    <option value="homePage">HOME</option>
                    <option value="aboutUs">WHO WE ARE</option>
                    <option value="works">WORKS</option>
                    <option value="contact">CONTACT</option>
                </select>
            </div>
        </div>
        <div id="homePage" class="page introWrap">
            <div id="introBg">
                <img src="assets/images/slider/bg1.jpg" alt="IntroBg" width="1920" height="1080" />
                <img src="assets/images/slider/bg2.jpg" alt="IntroBg" width="1920" height="1080" />
                <img src="assets/images/slider/bg3.jpg" alt="IntroBg" width="1920" height="1080" />
                <img src="assets/images/slider/bg4.jpg" alt="IntroBg" width="1920" height="1080" />
            </div>
            <div class="infoArea">
                <div class="container">
                    <div class="row-fluid">
                        <span class="infoTitle">WELCOME TO OUR SITE</span>
                    </div>
                    <div class="row-fluid">
                        <div class="span2 hidden-phone">
                        </div>
                        <p class="span8">
                            Very clean, minimal and perfect One Page Template.</br>
							It has been designed for digital agency, small business, landing page, show portfolio and more.</br>
							If you want to show your portfolio or introduce yourself we have designed in the best it for you.
						</p>
                        <div class="span2 hidden-phone">
                        </div>
                    </div>
                    <div class="row-fluid">
                        <a href="javascript:;" class="butn white" onclick="customScrollTo('aboutUs');">CLICK
                            FOR GET TO KNOW US</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="aboutUs" class="page pageContent">
            <div class="container">
                <div class="row-fluid">
                    <h1 class="pageTitle">
                        ABOUT US
                    </h1>
                </div>
                <div class="row-fluid">
                    <div class="span1">
                    </div>
                    <p class="span10">
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots
                        in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                        Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked
                        up one of the more obscure Latin words,theory of ethics, very popular during the
                        Renaissance.
                    </p>
                    <div class="span1">
                    </div>
                </div>
            </div>
            <div class="seperator">
                <div class="container">
                    <div class="row-fluid services">
                        <div class="span2">
                        </div>
                        <div class="span2 serviceItem">
                            <div class="serviceImage service1">
                            </div>
                            <span>SEO READY</span>
                        </div>
                        <div class="span2 serviceItem">
                            <div class="serviceImage service2">
                            </div>
                            <span>WEB DESIGN</span>
                        </div>
                        <div class="span2 serviceItem">
                            <div class="serviceImage service3">
                            </div>
                            <span>ADVERGAME</span>
                        </div>
                        <div class="span2 serviceItem">
                            <div class="serviceImage service4">
                            </div>
                            <span>DEVELOPMENT</span>
                        </div>
                        <div class="span2">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row-fluid">
                    <h1 class="pageTitle">
                        OUR TEAM
                    </h1>
                </div>
                <div class="spaceSep">
                </div>
                <div class="row-fluid teams">
                    <div class="span3 teamItem">
                        <img src="assets/images/team-1.jpg" alt="Team Item" />
                        <div class="mask">
                            <h3 class="teamName">
                                MICHAEL SCOFIELD</h3>
                            <span class="teamTitle">Software Developer</span> <span class="teamDesc hidden-tablet">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</span>
                            <ul class="teamSocial hidden-tablet">
                                <li><a class="fb" href="javascript:;">Facebook</a></li>
                                <li><a class="tw" href="javascript:;">Twitter</a></li>
                                <li><a class="lk" href="javascript:;">LinkedIn</a></li>
                                <li><a class="dr" href="javascript:;">Dribble</a></li>
                                <li><a class="pn" href="javascript:;">Pinterest</a></li>
                                <li><a class="fl" href="javascript:;">Flicker</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span3 teamItem">
                        <img src="assets/images/team-2.jpg" alt="Team Item" />
                        <div class="mask">
                            <h3 class="teamName">
                                BRAD BELLICK</h3>
                            <span class="teamTitle">Project Manager</span> <span class="teamDesc hidden-tablet">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</span>
                            <ul class="teamSocial hidden-tablet">
                                <li><a class="fb" href="javascript:;">Facebook</a></li>
                                <li><a class="tw" href="javascript:;">Twitter</a></li>
                                <li><a class="lk" href="javascript:;">LinkedIn</a></li>
                                <li><a class="dr" href="javascript:;">Dribble</a></li>
                                <li><a class="pn" href="javascript:;">Pinterest</a></li>
                                <li><a class="fl" href="javascript:;">Flicker</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span3 teamItem">
                        <img src="assets/images/team-3.jpg" alt="Team Item" />
                        <div class="mask">
                            <h3 class="teamName">
                                SARA TANCREDI</h3>
                            <span class="teamTitle">Front-End Developer</span> <span class="teamDesc hidden-tablet">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</span>
                            <ul class="teamSocial hidden-tablet">
                                <li><a class="fb" href="javascript:;">Facebook</a></li>
                                <li><a class="tw" href="javascript:;">Twitter</a></li>
                                <li><a class="lk" href="javascript:;">LinkedIn</a></li>
                                <li><a class="dr" href="javascript:;">Dribble</a></li>
                                <li><a class="pn" href="javascript:;">Pinterest</a></li>
                                <li><a class="fl" href="javascript:;">Flicker</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span3 teamItem">
                        <img src="assets/images/team-4.jpg" alt="Team Item" />
                        <div class="mask">
                            <h3 class="teamName">
                                LINCOLN BURROWS</h3>
                            <span class="teamTitle">Art Director</span> <span class="teamDesc hidden-tablet">Contrary
                                to popular belief, Lorem Ipsum is not simply random text. It has roots</span>
                            <ul class="teamSocial hidden-tablet">
                                <li><a class="fb" href="javascript:;">Facebook</a></li>
                                <li><a class="tw" href="javascript:;">Twitter</a></li>
                                <li><a class="lk" href="javascript:;">LinkedIn</a></li>
                                <li><a class="dr" href="javascript:;">Dribble</a></li>
                                <li><a class="pn" href="javascript:;">Pinterest</a></li>
                                <li><a class="fl" href="javascript:;">Flicker</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row-fluid teams">
                    <div class="span3 teamItem">
                        <img src="assets/images/team-5.jpg" alt="Team Item" />
                        <div class="mask">
                            <h3 class="teamName">
                                VERENICA DONOVAN</h3>
                            <span class="teamTitle">Software Developer</span> <span class="teamDesc hidden-tablet">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</span>
                            <ul class="teamSocial hidden-tablet">
                                <li><a class="fb" href="javascript:;">Facebook</a></li>
                                <li><a class="tw" href="javascript:;">Twitter</a></li>
                                <li><a class="lk" href="javascript:;">LinkedIn</a></li>
                                <li><a class="dr" href="javascript:;">Dribble</a></li>
                                <li><a class="pn" href="javascript:;">Pinterest</a></li>
                                <li><a class="fl" href="javascript:;">Flicker</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span3 teamItem">
                        <img src="assets/images/team-6.jpg" alt="Team Item" />
                        <div class="mask">
                            <h3 class="teamName">
                                FERNANDO SUCRE</h3>
                            <span class="teamTitle">Project Manager</span> <span class="teamDesc hidden-tablet">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</span>
                            <ul class="teamSocial hidden-tablet">
                                <li><a class="fb" href="javascript:;">Facebook</a></li>
                                <li><a class="tw" href="javascript:;">Twitter</a></li>
                                <li><a class="lk" href="javascript:;">LinkedIn</a></li>
                                <li><a class="dr" href="javascript:;">Dribble</a></li>
                                <li><a class="pn" href="javascript:;">Pinterest</a></li>
                                <li><a class="fl" href="javascript:;">Flicker</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span3 teamItem">
                        <img src="assets/images/team-7.jpg" alt="Team Item" />
                        <div class="mask">
                            <h3 class="teamName">
                                THEODORE BAGWELL</h3>
                            <span class="teamTitle">Front-End Developer</span> <span class="teamDesc hidden-tablet">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</span>
                            <ul class="teamSocial hidden-tablet">
                                <li><a class="fb" href="javascript:;">Facebook</a></li>
                                <li><a class="tw" href="javascript:;">Twitter</a></li>
                                <li><a class="lk" href="javascript:;">LinkedIn</a></li>
                                <li><a class="dr" href="javascript:;">Dribble</a></li>
                                <li><a class="pn" href="javascript:;">Pinterest</a></li>
                                <li><a class="fl" href="javascript:;">Flicker</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span3 teamItem">
                        <img src="assets/images/team-8.jpg" alt="Team Item" />
                        <div class="mask">
                            <h3 class="teamName">
                                SOFIA LUGO</h3>
                            <span class="teamTitle">Art Director</span> <span class="teamDesc hidden-tablet">Contrary
                                to popular belief, Lorem Ipsum is not simply random text. It has roots</span>
                            <ul class="teamSocial hidden-tablet">
                                <li><a class="fb" href="javascript:;">Facebook</a></li>
                                <li><a class="tw" href="javascript:;">Twitter</a></li>
                                <li><a class="lk" href="javascript:;">LinkedIn</a></li>
                                <li><a class="dr" href="javascript:;">Dribble</a></li>
                                <li><a class="pn" href="javascript:;">Pinterest</a></li>
                                <li><a class="fl" href="javascript:;">Flicker</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="seperator blockquote">
                <div class="prlxImage bg1">
                </div>
                <div class="container">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="noteWrap">
                                <span class="note">"EVERYTHING IS DESIGNED. FEW THINGS ARE DESIGNED WELL"</span>
                                <span class="noteAuthor">-Brian Occonner-</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="works" class="page pageContent">
            <div class="container">
                <div class="row-fluid">
                    <h1 class="pageTitle">
                        WORKS
                    </h1>
                </div>
                <div class="spaceSep">
                </div>
            </div>
            <div id="workItems">
            </div>
            <div class="seperator">
                <div class="container">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="noteWrap">
                                <span class="note">A site-wide Call to Action Section!</span> <a class="butn big black"
                                    href="javascript:;">BUY THIS THEME</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="contact" class="page pageContent">
            <div class="prlxImage bg2">
            </div>
            <div class="container">
                <div class="row-fluid">
                    <h1 class="pageTitle">
                        DO YOU WANT TO TOUCH US
                    </h1>
                </div>
                <div class="spaceSep">
                </div>
                <div class="row-fluid contact">
                    <div class="span6">
                        <form id="contact-form" action="contact.php" method="get">
                        <div class="row-fluid">
                            <p class="span12">
                                <label for="name">
                                    Your name</label>
                                <input type="text" id="name" name="name" class="required span12" maxlength="25" />
                            </p>
                        </div>
                        <div class="row-fluid">
                            <p class="span12">
                                <label for="email" class="second-color">
                                    E-mail</label>
                                <input type="text" id="email" name="email" class="required email span12" maxlength="25" />
                            </p>
                        </div>
                        <div class="row-fluid">
                            <p class="span12 multi">
                                <label for="message" class="second-color">
                                    Message</label>
                                <textarea id="message" name="message" class="required span12"></textarea>
                            </p>
                        </div>
                        <a href="javascript:;" class="butn white">SEND MESSAGE</a>
                        </form>
                    </div>
                    <div class="span6">
                        <div class="contactLogo">
                        </div>
                        <span class="contactTitle big">ONE PAGE SITE TEMPLATE</span> <span class="contactSep">
                        </span><span class="contactTitle">ADDRESS</span> <span class="contactDesc">145 Shelly
                            Lane Delran, New Jersey 08079</span> <span class="contactSep"></span><span class="contactTitle">
                                CONTACT INFO</span> <span class="contactDesc">1-856-237-8596</span> <span class="contactDesc m10">
                                    themes@prof.me</span>
                    </div>
                </div>
            </div>
            <div class="socialWrap">
                <div class="social">
                    <a class="fb" href="https://www.facebook.com/ProfMeThemes" target="_blank">Facebook</a>
                    <a class="tw" href="https://twitter.com/ProfMeThemes" target="_blank">Twitter</a>
                    <a class="lk" href="javascript:;" target="_blank">LinkedIn</a> <a class="dr" href="javascript:;"
                        target="_blank">Dribble</a> <a class="pn" href="javascript:;" target="_blank">Pinterest</a>
                    <a class="fl" href="javascript:;" target="_blank">Flicker</a>
                </div>
                <div class="copyText">
                    © 2013 YouSite. All Rights Reserved.
                </div>
            </div>
        </div>
    </div>
</body>
</html>
