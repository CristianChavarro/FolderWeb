<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Servicios extends Model
{
    protected $table = 'Servicios';

    protected $fillable = ['name', 'type'];

    protected $guarded = ['id'];

}
